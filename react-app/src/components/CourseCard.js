// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';

// function WithHeaderExample() {
//   return (
//     <Card>
//     <Card.Body>
//       <Card.Text as="h5">Sample Course</Card.Text>
      
//       {/*<Card.Text>Description:</Card.Text>*/}
//       <Card.Text>Description:</Card.Text>
//       <Card.Text>This is a sample course Offering</Card.Text>
       
//         <Card.Text>Price:</Card.Text>
//         <Card.Text> Php 40,0000</Card.Text>
//         <Button variant="primary">Enroll</Button>
//       </Card.Body>
//     </Card>
//   );
// }

// export default WithHeaderExample;

import {Link} from 'react-router-dom'

import {useState, useEffect} from 'react';
//import {useEffect} from 'react';

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

    // Deconstruct the course properties into their own variables
    const { name, description, price, _id } = course;

    /*
        SYNTAX:
            const [getter, stter] = useState(initialGetterValue);
    */
//   const [count, setCount] = useState(0);
// const [seats, setSeats] = useState(30);

//     function enroll () {
        
//  /*       if (limit > 0) {
//            setCount(count + 1);
//            setLimit(limit - 1);
//         } else {
//              alert ('no more seats')
//         }
//     };*/

//     setCount(count + 1);
//     setSeats(seats - 1);
// };

// useEffect(() =>{
//     if (seats <= 0) {
//         alert ("no more seats available")
//     }
// }, [seats]);



return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                      {/*  <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>
                    </Card.Body>*/}

                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )

}